import {useCallback, useState} from "react";
import useLocalStorage from "@src/hooks/useLocalStorage";

type UseFoldersResult = {
  setSource(value: string[]): void;
  setDest(value: string[]): void;
  source: string[],
  dest: string[],
}

const useFolders = (): UseFoldersResult => {

  const [storedSource, storeSource] = useLocalStorage("sourcePath", []);
  const [storedDest, storeDest] = useLocalStorage("destPath", []);
  const [source, setSourceValue] = useState<string[]>(storedSource);
  const [dest, setDestValue] = useState<string[]>(storedDest);

  const setSource = useCallback((value) => {
    setSourceValue(value);
    storeSource(value);
  }, [storeSource]);

  const setDest = useCallback((value) => {
    setDestValue(value);
    storeDest(value);
  }, [storeDest]);

  return {
    source,
    setSource,
    dest,
    setDest,
  };
}

export default useFolders;
