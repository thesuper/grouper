import {dialog, ipcMain} from "electron";
const fs = require('fs');
const path = require('path');

type Paths = {
  source: string[],
  dest: string[],
}

ipcMain.on("verify-paths", (event, paths: Paths) => {
  const {source, dest} = paths;
  const errors: string[] = [];
  [[source, "Source"], [dest, "Destination"]].forEach(([folder, title]) => {
    if (!folder[0] || !fs.existsSync(path.normalize(folder[0]))) {
      errors.push(`${title} folder is not accessible`);
    }
  });
  if (errors.length) {
    dialog.showErrorBox("Error", errors.join("\n"));
  }
  event.returnValue = !errors.length;
});
