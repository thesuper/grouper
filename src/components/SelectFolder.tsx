import {Button, Col, Input} from "antd";
import {FolderOpenOutlined} from "@ant-design/icons";
import React, {FC, useState} from "react";
const electron = window.require('electron');
const {remote}  = electron;
const {dialog} = remote;

interface SelectFolderProps {
  onSelect(value: string[]): void;
  label: string;
  labelSize?: number;
  inputSize?: number;
  buttonSize?: number;
  value: string;
}

const SelectFolder: FC<SelectFolderProps> = ({onSelect, value, inputSize = 18, buttonSize = 2, labelSize = 4, label}) => {
  const [pending, setPending] = useState(false);

  const handleSelectFolder = async () => {
    setPending(true);
    try {
      const result = await dialog.showOpenDialog({
        properties: ["openDirectory", "dontAddToRecent"],
        defaultPath: value ? value : undefined,
      });
      if (!result.canceled) {
        onSelect(result.filePaths);
      }
    } catch (err) {
      dialog.showErrorBox("Error", err);
    } finally {
      setPending(false);
    }
  }

  return (<>
    <Col className="valign-center" span={labelSize}>
      <span>{label}</span>
    </Col>
    <Col span={inputSize}>
      <Input
        readOnly
        onClick={pending ? undefined : handleSelectFolder}
        value={value}
      />
    </Col>
    <Col span={buttonSize}>
      <Button
        className="full-width"
        disabled={pending}
        onClick={handleSelectFolder}>
          <FolderOpenOutlined />
      </Button>
    </Col>
  </>);
}

export default SelectFolder;
