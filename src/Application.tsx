import "./Application.less";
import {Button, Col, Layout, Row} from "antd";
import { hot } from "react-hot-loader";
import React, {useCallback, useState} from "react";
import SelectFolder from "@src/components/SelectFolder";
import useFolders from "@src/hooks/useFolders";
const electron = window.require('electron');
const {remote, ipcRenderer}  = electron;
const {dialog} = remote;

type AppProps = {
  title: string;
  version: string;
};

const Application: React.FC<AppProps> = (props) => {

  const {source, dest, setDest, setSource} = useFolders();
  const [busy, setBusy] = useState(false);

  const handleExecute = useCallback(() => {
    const result = ipcRenderer.sendSync("verify-paths", {
      source,
      dest,
    });
    if (result) {
      setBusy(true);
    }
  }, [dest, source]);

  const submitDisabled = !source.length || !dest.length;
  return (
    <Layout>
      <Layout.Content className="full-page-content">
        <Row gutter={[0, 16]}>
          <SelectFolder
            label="Source folder"
            onSelect={setSource}
            value={source ? source[0] : "..."}
          />
          <SelectFolder
            label="Destination folder"
            onSelect={setDest}
            value={dest ? dest[0] : "..."}
          />
          <Col span={20} />
          <Col span={4}>
            <Button
              className="full-width"
              disabled={submitDisabled}
              loading={busy}
              onClick={handleExecute}
              type="primary"
            >{busy ? "Executing" : "Execute"}</Button>
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
};

export default hot(module)(Application);
